package com.sscy.newsscrap.controler;

import com.sscy.newsscrap.Service.MoneyService;
import com.sscy.newsscrap.model.MoneyChangeRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping ("/money")
@RequiredArgsConstructor
public class MoneyControler {

    private final MoneyService moneyservice;

    @PostMapping("/change")
    public String peoplechange(@RequestBody MoneyChangeRequest request) {
        String result = moneyservice.convertMoney(request.getMoney());
        return result;
    }

    @GetMapping("/pay-back")
    public String peolepayback() {
        return  "환불되었습니다. 고객님";
    }
}



